<?php
	$css_external = 'css/pengguna.php';
	$js_external = 'js/pengguna.php';

	include '../connection.php';
	include '../template/head.php';
	include '../template/page_head.php';
	include '../template/sidebar.php';

	$query = mysql_query('SELECT * FROM tb_user JOIN tb_karyawan ON tb_karyawan.nip = tb_user.nip');
	$getKaryawan = mysql_query('SELECT nip, nama_karyawan FROM tb_karyawan');
?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Data Master Pengguna</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div>
							<span class="pull-left">
								<strong>Daftar Pengguna</strong>
							</span>
							<span class="pull-right">
								<button type="button" class="btn btn-primary manipulasi" data-toggle="modal" data-target="#myModal">
									<i class="glyphicon glyphicon-plus"></i>Tambah
								</button>
							</span>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-striped" id="example">
							<thead>
								<tr>
									<th>Nama Karyawan</th>
									<th>Username</th>
									<th>Password</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($value = mysql_fetch_array($query)) { 
									$status = ($value['status'] == 1) ? 'Administrator' : 'User';
								?>
									<tr>
										<td><?php echo $value['nama_karyawan'] ?></td>
										<td><?php echo $value['username'] ?></td>
										<td><?php echo $value['password'] ?></td>
										<td><?php echo $status ?></td>
										<td>
											<a href="javascript:;" data-toggle="modal" data-target="#myModal" class="manipulasi" id="<?php echo $value['id'] ?>">Ubah</a> | 
											<a href="process/crud_pengguna.php?kode=<?php echo $value['id'] ?>&_method=delete" onclick="return confirm('Yakin ingin di hapus?')">Hapus</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->

	<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<form id="form_pengguna" method="post" action="process/crud_pengguna.php" onsubmit="return check()">
					<input type="hidden" name="_method" id="method">
					<input type="hidden" name="id" id="id">
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-nama">Nama Karyawan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<select name="nama" id="input-nama" class="form-control" required>
								<option value="" disabled selected> - Pilih Karyawan - </option>
								<?php while ($data = mysql_fetch_array($getKaryawan)) { ?>
									<option value="<?php echo $data['nip'] ?>"> <?php echo $data['nama_karyawan'] ?> </option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-user">Username</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="user" id="input-user" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-pw1">Password</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="pw1" id="input-pw1" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-pw2">Ketik Ulang Password</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="password" name="pw2" id="input-pw2" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-status">Status</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<select name="status" id="input-status" class="form-control" required>
								<option value="" disabled selected> - Pilih Status - </option>
								<option value="1"> Administrator </option>
								<option value="2"> User </option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success" form="form_pengguna" name="button_submit">Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<?php include '../template/footer.php'; ?>