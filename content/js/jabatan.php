<script type="text/javascript" src="../assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../assets/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#example').DataTable();

		$('.manipulasi').click(function(){
			$('#form_karyawan')[0].reset();
			if ($(this).attr('id') != null) {
				$('.modal-title').text('Ubah Data Jabatan');
				$('#method').val('update');

				var kode = $(this).attr('id');

				$.ajax({
					url:'process/crud_jabatan.php',
					type: 'get',
					data: {kode:kode, _method:'get_where'},
					dataType: 'json',
					success: function(data){
						$('#input-jabatan').attr('readonly', true);

						$('#input-jabatan').val(data.kode_jabatan);
						$('#input-nama').val(data.nama_jabatan);
						$('#input-gapok').val(data.tunjangan_jabatan);
					},
					error: function(){
						alert('error getting data');
					}
				});
			}
			else {
				$('#input-jabatan').attr('readonly', false);
				$('#method').val('insert');
				$('.modal-title').text('Tambah Data Jabatan');
			}
		});
	})
</script>