<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
	var terlambat = new Array();

	$('#select2').select2();

	function initTimePicker(){
		$(".masuk").timepicker({
			minuteStep: 1,
			showMeridian: false
		});

		$(".keluar").timepicker({
			minuteStep: 1,
			showMeridian: false
		});
	}

	initTimePicker();

	function hitungPajak(){
		var gajiHarian = 0;
		$(".detail tr").each(function(){
			gajiHarian += parseInt($(this).find(".transport").val() || 0) + parseInt($(this).find(".lembur").val() || 0);
		});
		var gapok = parseInt($('.gaji_pokok').text().unformat() || 0);
		var tj_jabatan = parseInt($('.tunjangan_jabatan').text().unformat() || 0);
		var tj_bpjs = parseInt($('input[name="tj_bpjs"]').val() || 0);
		var tj_bonus = parseInt($('input[name="tj_bonus"]').val() || 0);
		var tj_lainnya = parseInt($('input[name="tj_lainnya"]').val() || 0);
		var iu_kesehatan = parseInt($('input[name="iu_kesehatan"]').val() || 0);
		var iu_tenagakerja = parseInt($('input[name="iu_tenagakerja"]').val() || 0);
		var sp = parseInt($('.sp').val() || 0);

		var totalGaji = gajiHarian + gapok + tj_jabatan + tj_bpjs + tj_bonus + tj_lainnya - iu_kesehatan - iu_tenagakerja - sp;
		if(totalGaji > 5000000){
			totalGaji = totalGaji * 0.05;
			$('.pajak').text("Rp. " + totalGaji.format());
			$('input[name="pajak"]').val(totalGaji);
		}else{
			$('.pajak').text("Rp. " + 0);
			$('input[name="pajak"]').val(0);
		}
	}

	function hitungDendaSP(){
		var current_sp = parseInt($('input[name="real_sp"]').val() || 0);
		var current_tgl_sp = $('input[name="real_tgl_sp"]').val();

		var _current_sp = parseInt($('input[name="sp"]').val());
		var _current_tgl_sp = $('input[name="tgl_sp"]').val();

		if(terlambat.length == 5){
			_current_sp = current_sp + 1;
			_current_tgl_sp = $($(".detail tr")[1]).find('.tanggal').val();

			$('input[name="sp"]').val(_current_sp);
			$('input[name="tgl_sp"]').val(_current_tgl_sp);
		}else if(terlambat.length == 4){
			if(_current_sp > current_sp){
				$('input[name="sp"]').val(_current_sp-1);
			}
		}

		if(_current_sp > 0 && _current_tgl_sp != ""){
			var tgl_sp = new Date(_current_tgl_sp);
			var tgl_sekarang = new Date($($(".detail tr")[1]).find('.tanggal').val());
			var tgl_endSp = new Date(_current_tgl_sp);
			tgl_endSp.setMonth(tgl_sp.getMonth() + 3);

			if(tgl_sekarang.getTime() < tgl_endSp.getTime()){
				switch(_current_sp){
					case 1:
						$('.sp').text("Rp. " + parseInt(100000).format());
						$('input[name="potongan"]').val(100000);
						break;
					case 2:
						$('.sp').text("Rp. " + parseInt(200000).format());
						$('input[name="potongan"]').val(200000);
						break;
				}
			}else{
				$('.sp').text("Rp. " + 0);
				$('input[name="potongan"]').val(0);
			}
		}else{
			$('.sp').text("Rp. " + 0);
			$('input[name="potongan"]').val(0);
		}
	}

	function calculateAll(){
		hitungPajak();
		hitungDendaSP();
	}

	function delRow(el){
		$(el).parents("tr").remove();
		calculateAll();
	}

	function hitungTelat(el){
		var parent = $(el).parents("tr");
		var masukVal = parent.find(".masuk").val();

		var arrMasuk = masukVal.split(":");
		var masuk = new Date();
		masuk.setHours(arrMasuk[0]);
		masuk.setMinutes(arrMasuk[1]);
		masuk.setSeconds(0);

		var seharusnya = new Date();
		seharusnya.setHours(8);
		seharusnya.setMinutes(30);
		seharusnya.setSeconds(0);
		
		var id = $(".detail tr").index(parent);

		if(masuk.getTime() > seharusnya.getTime()){
			parent.find(".transport").val(0);
			if(terlambat.indexOf(id) < 0 && parent.find(".ketTelat").val() == ""){
				terlambat.push(id);
			}else if(parent.find(".ketTelat").val() != ""){
				terlambat.removeByVal(id);
			}
			parent.find(".ketTelat").show();
		}else{
			parent.find(".transport").val(15000);
			if(terlambat.indexOf(id) >= 0){
				terlambat.removeByVal(id);
			}
			parent.find(".ketTelat").hide();
			parent.find(".ketTelat").val("");
		}

		calculateAll();
	}

	function hitungLembur(el){
		var parent = $(el).parents("tr");
		var keluarVal = parent.find(".keluar").val();

		var arrKeluar = keluarVal.split(":");
		var keluar = new Date();
		keluar.setHours(arrKeluar[0]);
		keluar.setMinutes(arrKeluar[1]);
		keluar.setSeconds(0);

		var seharusnya = new Date();
		seharusnya.setHours(17);
		seharusnya.setMinutes(0);
		seharusnya.setSeconds(0);

		var gapok = $('.gaji_pokok').text().unformat() / 160;

		if(keluar.getTime() > seharusnya.getTime()){
			parent.find(".lembur").val(1.5 * gapok);

			var hh = Math.ceil((keluar.getTime() - seharusnya.getTime())/1000/60/60);
			if(hh >= 1){
				parent.find(".lembur").val((hh+1) * gapok);
			}
		}else{
			parent.find(".lembur").val(0);
		}

		calculateAll();
	}


	$('.karyawan').on('change', function () {
	    var selectData = $(this).val();
	    $.ajax({
	        type: "POST",
	        url: "process/crud_transaksi.php",
	        dataType: 'json',
	        data: {
	        	'tabel'	: "karyawan",
	        	'nip'	: $('.karyawan').val()
	        },
	        success: function(data) {
	            $('.gaji_pokok').text("Rp. " + parseInt(data.gaji_pokok || 0).format());
	            $('.tunjangan_jabatan').text("Rp. " + parseInt(data.tunjangan_jabatan || 0).format());
	            $('input[name="real_sp"]').val(data.status);
	            $('input[name="real_tgl_sp"]').val(data.tgl_sp);
	            $('input[name="sp"]').val(data.status);
	            $('input[name="tgl_sp"]').val(data.tgl_sp);

	    		calculateAll();
	        }
	    });
	});

	$(".addRow").on('click', function(){
		var index = $(".detail tr").length - 1;
		var lastDate = new Date($($(".detail tr")[index]).find(".tanggal").val());
		lastDate.setDate(lastDate.getDate() + 1);
		var currDate = lastDate.getFullYear() + "/" + (lastDate.getMonth() + 1) + "/" + (lastDate.getDate() + 1);
        var row = "<tr>";
    	row += "<td>";
		row += '<div class="input-group date" data-provide="datepicker" data-date-format="yyyy/mm/dd" data-date-autoclose="true">';
		row += '<input  style="text-align: center " value="' + lastDate.shortDate() + '" required="" class="form-control tanggal" type="text" name="tanggal[' + index + ']" />';
		row += '<div class="input-group-addon">';
		row += '<span class="glyphicon glyphicon-calendar"></span>';
		row += '</div></div></td>';
		row += '<td><input required="" style="text-align: center" class="form-control masuk" type="text" onchange="javascript:hitungTelat(this);" name="masuk[' + index + ']" value="8:00" /><input style="display: none" onchange="javascript:hitungTelat(this);" class="form-control ketTelat" type="text" name="ket[' + index + ']" value="" placeholder="Keterangan Telat" /></td>';
		row += '<td><input required="" style="text-align: center" class="form-control keluar" type="text" onchange="javascript:hitungLembur(this);" name="keluar[' + index + ']" value="17:00" /></td>';
		row += '<td><input required="" style="text-align: right" class="form-control transport" type="text" name="transport[' + index + ']" value="15000" /></td>';
		row += '<td><input required="" style="text-align: right" class="form-control lembur" type="text" name="lembur[' + index + ']" value="0" /></td>';
		row += '<td style="vertical-align:middle"><button class="btn btn-xs btn-danger" onclick="javascript:delRow(this);"><i class="glyphicon glyphicon-remove"></i></button></td>';
        row += "</tr>";
        $(".detail").append(row)
        initTimePicker();

        calculateAll();
	});
</script>