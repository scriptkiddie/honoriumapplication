<script type="text/javascript" src="../assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../assets/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#example').DataTable();

		$('.manipulasi').click(function(){
			$('#form_karyawan')[0].reset();

			if ($(this).attr('id') != null) {
				$('#method').val('update');
				$('.modal-title').text('Ubah Data Karyawan');

				var nip = $(this).attr('id');

				$.ajax({
					url:'process/crud_karyawan.php',
					type: 'get',
					data: {nip:nip, _method:'get_where'},
					dataType: 'json',
					success: function(data){
						$('#input-nip').attr('readonly', true);

						$('#input-nip').val(data.nip);
						$('#input-nama').val(data.nama_karyawan);
						$('#input-jk').val(data.jenis_kelamin).change();
						$('#input-alamat').val(data.alamat);
						$('#input-telp').val(data.telepon);
						$('#input-email').val(data.email);
						$('#input-jabatan').val(data.kode_jabatan).change();
						$('#input-gapok').val(data.gaji_pokok).change();
						$('#input-status').val(data.status_karyawan).change();
					},
					error: function(){
						alert('error getting data');
					}
				});
			}
			else {
				$('#input-nip').attr('readonly', false);
				$('#method').val('insert');
				$('.modal-title').text('Tambah Data Karyawan');
			}
		});
	})
</script>