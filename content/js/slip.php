<script type="text/javascript">
	$('#select_year').change(function(){
		$('#select_month').empty();
		var tahun = $(this).val();
		var bulan = ['','Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

		$.ajax({
			url: 'process/slip_json.php',
			type: 'post',
			data: {type: 'getMonth', year:tahun},
			dataType: 'json',
			success: function(data){
				$('#select_month').append('<option value="" selected> - Silahkan Pilih Bulan - </option>');
				$.each(data, function(key, val){
					$('#select_month').append('<option value="' + val.bulan + '"> ' + bulan[val.bulan] + ' </option>');
				});

			},
			error: function(){
				alert('error getting data');
			}
		})
	});

	$('#select_month').change(function(){
		if ($(this).val() == '') {
			$('#tampil').prop('disabled', true);
		}
		else{
			$('#tampil').prop('disabled', false);
		}
	});

	$('#tampil').click(function(){
		var tahun = $('#select_year').val();
		var bulan = $('#select_month').val();

		$.ajax({
			url: 'process/slip_json.php',
			type: 'post',
			data: {type: 'getSlip', year:tahun, month:bulan},
			dataType: 'json',
			success: function(data){
				// console.log(data);
				// $('#titel').text();
				// $('#penerimaan').append();
				// $('#pengurangan').append();
				// $('#diterima').text();

				$('#title').text("Periode " + $('#select_month option:selected').text() + " " + tahun);
				$('#gapok').text("Rp. " + parseInt(data.gaji_pokok).format());
				$('#tj_jabatan').text("Rp. " + parseInt(data.tunjangan_jabatan).format());
				$('#tj_transport').text("Rp. " + parseInt(data.transport).format());
				$('#tj_bpjs').text("Rp. " + parseInt(data.tj_bpjs).format());
				$('#tj_lembur').text("Rp. " + parseInt(data.lembur).format());
				$('#tj_bonus').text("Rp. " + parseInt(data.tj_bonus).format());
				$('#tj_lainnya').text("Rp. " + parseInt(data.tj_lainnya).format());
				var penerimaan = parseInt(data.gaji_pokok) + parseInt(data.tunjangan_jabatan) + parseInt(data.transport) + parseInt(data.tj_bpjs) + parseInt(data.lembur) + parseInt(data.tj_bonus) + parseInt(data.tj_lainnya);
				$('#total_penerimaan').text("Rp. " + parseInt(penerimaan).format());

				$('#iu_kesehatan').text("Rp. " + parseInt(data.iu_kesehatan).format());
				$('#iu_tenagakerja').text("Rp. " + parseInt(data.iu_tenagakerja).format());
				$('#pajak').text("Rp. " + parseInt(data.pajak).format());
				$('#telat').text("Rp. " + parseInt(data.potongan).format());
				var pengurangan = parseInt(data.iu_kesehatan) + parseInt(data.iu_tenagakerja) + parseInt(data.pajak) + parseInt(data.potongan);
				$('#total_pengurangan').text("Rp. " + parseInt(pengurangan).format());
				
				$('#diterima').text("Rp. " + parseInt(penerimaan - pengurangan).format());

				$('.slip_gaji').show();
				$('.print').click(function(){
					 window.open('print_slip.php?year=' + tahun + '&month=' + bulan,'_blank');
				});
			},
			error: function(){
				alert('error getting data');
			}
		})
	});
</script>