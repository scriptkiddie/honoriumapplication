<script type="text/javascript" src="../assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../assets/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#example').DataTable();

		$('.manipulasi').click(function(){
			$('#form_pengguna')[0].reset();
			if ($(this).attr('id') != null) {
				$('.modal-title').text('Ubah Data Pengguna');
				$('#method').val('update');

				var kode = $(this).attr('id');

				$.ajax({
					url:'process/crud_pengguna.php',
					type: 'get',
					data: {kode:kode, _method:'get_where'},
					dataType: 'json',
					success: function(data){
						$('#id').val(data.id);
						$('#input-nama').val(data.nip).change();
						$('#input-user').val(data.username);
						$('#input-pw1').val(data.password);
						$('#input-status').val(data.status).change();
					},
					error: function(){
						alert('error getting data');
					}
				});
			}
			else {
				$('#method').val('insert');
				$('.modal-title').text('Tambah Data Pengguna');
			}
		});
	})
</script>