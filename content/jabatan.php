<?php
	$css_external = 'css/jabatan.php';
	$js_external = 'js/jabatan.php';

	include '../connection.php';
	include '../template/head.php';
	include '../template/page_head.php';
	include '../template/sidebar.php';

	$query = mysql_query('SELECT * FROM tb_jabatan');
?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Data Master Jabatan</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-defautl">
					<div class="panel-heading">
						<div>
							<span class="pull-left">
								<strong>Daftar Jabatan</strong>
							</span>
							<span class="pull-right">
								<button type="button" class="btn btn-primary manipulasi" data-toggle="modal" data-target="#myModal">
									<i class="glyphicon glyphicon-plus"></i>Tambah
								</button>
							</span>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-striped" id="example">
							<thead>
								<tr>
									<th>Kode Jabatan</th>
									<th>Nama Jabatan</th>
									<th>Tunjangan Jabatan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php while($value = mysql_fetch_array($query)) { ?>
									<tr>
										<td><?php echo $value['kode_jabatan'] ?></td>
										<td><?php echo $value['nama_jabatan'] ?></td>
										<td><?php echo $value['tunjangan_jabatan'] ?></td>
										<td>
											<a href="javascript:;" data-toggle="modal" data-target="#myModal" class="manipulasi" id="<?php echo $value['kode_jabatan'] ?>">Ubah</a> | 
											<a href="process/crud_jabatan.php?kode=<?php echo $value['kode_jabatan'] ?>&_method=delete" onclick="return confirm('Yakin ingin hapus?')">Hapus</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->

	<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<form id="form_karyawan" method="post" action="process/crud_jabatan.php">
					<input type="hidden" name="_method" id="method">
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-jabatan">Kode Jabatan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="kode" id="input-jabatan" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-nama">Nama Jabatan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="nama" id="input-nama" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-gapok">Tunjangan Jabatan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="number" name="gapok" id="input-gapok" class="form-control" required>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" name="button_submit" form="form_karyawan" class="btn btn-success">Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<?php include '../template/footer.php'; ?>