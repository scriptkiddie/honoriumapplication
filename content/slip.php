<?php
	include '../connection.php';
	include '../template/head.php';
	include '../template/page_head.php';
	include '../template/sidebar.php';
	$js_external = 'js/slip.php';

	$tahun = mysql_query("SELECT YEAR(periode) AS tahun FROM tb_header_absen WHERE nip = '" . $_SESSION['nomor_ip'] . "' GROUP BY YEAR(periode)");
?>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Slip Gaji</h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-2 col-xs-12 form-group">
								<label>Pilih Tahun</label>
							</div>
							<div class="col-lg-4 col-xs-12 form-group">
								<select class="form-control" id="select_year">
									<option value="" selected> - Silahkan Pilih Tahun - </option>
									<?php while($year = mysql_fetch_array($tahun)) { ?>
										<option value="<?php echo $year['tahun'] ?>"> <?php echo $year['tahun'] ?> </option>
									<?php } ?>
								</select>
							</div>
							<div class="col-lg-2 col-xs-12">
								<label>Pilih Bulan</label>
							</div>
							<div class="col-lg-4 col-xs-12">
								<select class="form-control" id="select_month">
									<option value="" selected> - Silahkan Pilih Bulan - </option>
								</select>
							</div>
						</div>
						<button class="btn btn-primary pull-right" id="tampil" disabled><i class="glyphicon glyphicon-search"></i>&emsp;Tampilkan</button>
					</div>
				</div>
			</div>	
		</div><!--/.row-->

		<div class="row slip_gaji" style="display:none;">
			<div class="col-lg-12">
				<div class="panel panel-default" id="tampil-slip">
					<div class="panel-heading">
						<div>
							<span class="pull-left"><h3 id="title">XXXX</h3></span>
							<span class="pull-right"><button id="print" class="btn btn-default print"><i class="glyphicon glyphicon-print"></i>Cetak</button></span>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table">
							<!-- <tr style="background-color:#6C7A89;color:white;">
								<th colspan="3" style="text-align:right">XXXX</th>
							</tr> -->
							<tr style="background-color:#ECF0F1;">
								<th colspan="4">Penerimaan</th>
							</tr>
							<tr>
								<td style="text-align:center">1</td>
								<td>Gaji Pokok</td>
								<td style="text-align:right" id="gapok">####</td>
							</tr>
							<tr>
								<td style="text-align:center">2</td>
								<td>Tunjangan Jabatan</td>
								<td style="text-align:right" id="tj_jabatan">####</td>
							</tr>
							<tr>
								<td style="text-align:center">3</td>
								<td>Tunjangan Transportasi</td>
								<td style="text-align:right" id="tj_transport">####</td>
							</tr>
							<tr>
								<td style="text-align:center">4</td>
								<td>Tunjangan BPJS</td>
								<td style="text-align:right" id="tj_bpjs">####</td>
							</tr>
							<tr>
								<td style="text-align:center">5</td>
								<td>Lembur</td>
								<td style="text-align:right" id="tj_lembur">####</td>
							</tr>
							<tr>
								<td style="text-align:center">6</td>
								<td>Bonus / THR</td>
								<td style="text-align:right" id="tj_bonus">####</td>
							</tr>
							<tr>
								<td style="text-align:center">7</td>
								<td>Penerimaan Lainnya</td>
								<td style="text-align:right" id="tj_lainnya">####</td>
							</tr>
							<tr style="background-color:grey;color:white">
								<th colspan="2">Total Penerimaan</th>
								<th style="text-align:right" id="total_penerimaan">####</th>
							</tr>
							<tr style="background-color:#fff;">
								<th colspan="4"></th>
							</tr>
							<tr style="background-color:#ECF0F1;">
								<th colspan="4">Pengurangan</th>
							</tr>
							<tr>
								<td style="text-align:center">1</td>
								<td>Iuran BPJS Kesehatan</td>
								<td style="text-align:right" id="iu_kesehatan">####</td>
							</tr>
							<tr>
								<td style="text-align:center">2</td>
								<td>Iuran BPJS Ketenagakerjaan</td>
								<td style="text-align:right" id="iu_tenagakerja">####</td>
							</tr>
							<tr>
								<td style="text-align:center">3</td>
								<td>Pajak</td>
								<td style="text-align:right" id="pajak">####</td>
							</tr>
							<tr>
								<td style="text-align:center">4</td>
								<td>Potongan Telat</td>
								<td style="text-align:right" id="telat">####</td>
							</tr>
							<tr style="background-color:grey;color:white">
								<th colspan="2">Total Pengurangan</th>
								<th style="text-align:right" id="total_pengurangan">####</th>
							</tr>
							<tr style="background-color:black;color:white">
								<th colspan="2">Total Diterima Karyawan</th>
								<th style="text-align:right" id="diterima">####</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	<!--/.main-->

<?php include '../template/footer.php'; ?>