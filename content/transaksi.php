<?php
	$css_external = 'css/transaksi.php';
	$js_external = 'js/transaksi.php';

	include '../connection.php';
	include '../template/head.php';
	include '../template/page_head.php';
	include '../template/sidebar.php';

	$karyawan = mysql_query('SELECT * FROM tb_karyawan');

	if(isset($_GET['message'])){
		echo "<script type='text/javascript'>alert('" . $_GET['message'] . "')</script>";
	}
?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Form Input Absen</h1>
			</div>
		</div><!--/.row-->
		<form method="post" action="process/crud_transaksi.php">
			<input type="hidden" name="real_sp" value="0" />
			<input type="hidden" name="real_tgl_sp" value="0" />

			<input type="hidden" name="sp" value="0" />
			<input type="hidden" name="tgl_sp" value="0" />

			<input type="hidden" name="pajak">
			<input type="hidden" name="potongan">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-defautl">
						<div class="panel-body">
							<div class="row col-xs-5">
								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Karyawan</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<select id="select2" required="" class="karyawan form-control" name="nip">
											<option disabled="" selected="">-- Pilih Karyawan --</option>
											<?php while($value = mysql_fetch_array($karyawan)) { ?>
											<option value="<?=$value['nip']?>"><?=$value['nama_karyawan']?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Gaji Pokok</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<label style="text-align: right " class="gaji_pokok form-control">Rp. 0</label>	
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Tunjangan Jabatan</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<label style="text-align: right " class="tunjangan_jabatan form-control">Rp. 0</label>	
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Tunjangan BPJS</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<input required="" style="text-align: right " onchange="javascript:calculateAll();" class="form-control" type="text" name="tj_bpjs" value="220000" />
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Bonus / THR</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<input required="" style="text-align: right " onchange="javascript:calculateAll();" class="form-control" type="text" name="tj_bonus" value="0" />
									</div>
								</div>
							</div>

							<div class="row col-xs-2"></div>

							<div class="row col-xs-5">
								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Lainnya</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<input required="" style="text-align: right " onchange="javascript:calculateAll();" class="form-control" type="text" name="tj_lainnya" value="0" />
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">BPJS Kesehatan</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<input required="" style="text-align: right " onchange="javascript:calculateAll();" class="form-control" type="text" name="iu_kesehatan" value="90000" />
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">BPJS Ketenagakerjaan</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<input required="" style="text-align: right " onchange="javascript:calculateAll();" class="form-control" type="text" name="iu_tenagakerja" value="145000" />
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Pajak PPh 5%</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<label style="text-align: right " class="pajak form-control">Rp. 0</label>	
									</div>
								</div>

								<div class="row form-group">
									<div class="col-lg-4 col-xs-12">
										<label for="input-email">Potongan SP</label>
									</div>
									<div class="col-lg-8 col-xs-12">
										<label style="text-align: right " class="sp form-control">Rp. 0</label>	
									</div>
								</div>
							</div>

							<div class="row col-xs-12">&nbsp;</div>

							<div class="row col-xs-12">
								<table class="table table-bordered detail" width="100%">
									<tr>
										<th>Tanggal</th>
										<th>Jam Masuk</th>
										<th>Jam Keluar</th>
										<th>Tunjangan Transport</th>
										<th colspan="2">Honor Lembur</th>
									</tr>

									<?php
									$totalHari = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
									for($i=0; $i < $totalHari; $i++){
										$date = mktime(0,0,0, date("m"), date("d", strtotime("+" . ($i) . " day", strtotime("01"))), date("Y"));
										if(date('w', $date) == 6 || date('w', $date) == 0){
											continue;
										}
									?>
									<tr>
										<td>
											<div class="input-group date" data-provide="datepicker" data-date-format="yyyy/mm/dd" data-date-autoclose="true">
												<input required="" value="<?=date("Y/m/d", $date)?>" style="text-align: center " class='form-control tanggal' type="text" name="tanggal[<?=$i?>]" />
												<div class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</div>
											</div>
										</td>
										<td>
											<input required="" onchange="javascript:hitungTelat(this);" style="text-align: center" class="form-control masuk" type="text" name="masuk[<?=$i?>]" value="8:00" />
											<input style="display: none" onchange="javascript:hitungTelat(this);" class="form-control ketTelat" type="text" name="ket[<?=$i?>]" value="" placeholder="Keterangan Telat" />
										</td>
										<td><input required="" onchange="javascript:hitungLembur(this);" style="text-align: center" class="form-control keluar" type="text" name="keluar[<?=$i?>]" value="17:00" /></td>
										<td><input required="" style="text-align: right" class="form-control transport" type="text" name="transport[<?=$i?>]" value="15000" /></td>
										<td><input required="" style="text-align: right" class="form-control lembur" type="text" name="lembur[<?=$i?>]" value="0" /></td>
										<?php if($i==0){ ?>
										<td style="vertical-align:middle"><button disabled="" class="btn btn-xs" style="background: #fff"><i style="color:#fff" class="glyphicon glyphicon-remove"></i></button></td>
										<?php }else{ ?>
										<td style="vertical-align:middle"><button type="button" onclick="javascript:delRow(this);" class="btn btn-danger btn-xs"><i style="color:#fff" class="glyphicon glyphicon-remove"></i></button></td>
										<?php }?>
									</tr>
									<?php } ?>
								</table>
							</div>

							<div class="row col-xs-12" style="text-align: right">
								<input type="submit" name="button_submit" class="btn btn-success" value="Simpan Record">
								<button type="button" class="btn btn-primary addRow">Tambah Inputan</button>
							</div>
							<div class="row col-xs-12" style="text-align: right"></div>
						</div>
					</div>
			</div>
		</div>
	</form>
</div>
<?php include '../template/footer.php'; ?>