<?php
	$css_external = 'css/karyawan.php';
	$js_external = 'js/karyawan.php';

	include '../connection.php';
	include '../template/head.php';
	include '../template/page_head.php';
	include '../template/sidebar.php';

	$query = mysql_query('SELECT * FROM tb_karyawan JOIN tb_jabatan ON tb_jabatan.kode_jabatan = tb_karyawan.kode_jabatan');
	$getJabatan = mysql_query('SELECT * FROM tb_jabatan');
?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Data Master Karyawan</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-defautl">
					<div class="panel-heading">
						<div>
							<span class="pull-left">
								<strong>Daftar Karyawan</strong>
							</span>
							<span class="pull-right">
								<button type="button" class="btn btn-primary manipulasi" data-toggle="modal" data-target="#myModal">
									<i class="glyphicon glyphicon-plus"></i>Tambah
								</button>
							</span>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-striped" id="example">
							<thead>
								<tr>
									<th>NIP</th>
									<th>Nama Karyawan</th>
									<th>Jabatan</th>
									<th>Telepon</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php while($value = mysql_fetch_array($query)) { ?>
									<tr>
										<td><?php echo $value['nip'] ?></td>
										<td><?php echo $value['nama_karyawan'] ?></td>
										<td><?php echo $value['nama_jabatan'] ?></td>
										<td><?php echo $value['telepon'] ?></td>
										<td>
											<a href="javascript:;" data-toggle="modal" data-target="#myModal" class="manipulasi" id="<?php echo $value['nip'] ?>">Ubah</a> | 
											<a href="process/crud_karyawan.php?nip=<?php echo $value['nip'] ?>&_method=delete" onclick="return confirm('Yakin ingin di hapus?')">Hapus</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->

	<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<form id="form_karyawan" method="post" action="process/crud_karyawan.php">
					<input type="hidden" name="_method" id="method">
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-nip">NIP</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="nip" id="input-nip" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-nama">Nama Karyawan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="nama" id="input-nama" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-jk">Jenis Kelamin</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<select name="jenis_kelamin" id="input-jk" class="form-control" required>
								<option value="" disabled selected> - Pilih Jenis Kelamin - </option>
								<option value="1"> Laki-laki </option>
								<option value="2"> Perempuan </option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-alamat">Alamat</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<textarea name="alamat" id="input-alamat" class="form-control" required></textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-telp">No. Telepon</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="text" name="telepon" id="input-telp" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-email">Email</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="email" name="email" id="input-email" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-jabatan">Jabatan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<select name="jabatan" id="input-jabatan" class="form-control" required>
								<option value="" disabled selected> - Pilih Jabatan - </option>
								<?php while ($data = mysql_fetch_array($getJabatan)) { ?>
									<option value="<?php echo $data['kode_jabatan'] ?>"> <?php echo $data['nama_jabatan'] ?> </option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-gapok">Gaji Pokok</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<input type="number" name="gapok" id="input-gapok" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-lg-3 col-xs-12">
							<label for="input-status">Status Karyawan</label>
						</div>
						<div class="col-lg-9 col-xs-12">
							<select name="status" id="input-status" class="form-control" required>
								<option value="" disabled selected> - Pilih Status Kekaryawanan - </option>
								<option value="0"> Karyawan Tetap </option>
								<option value="1"> Karyawan Kontrak </option>
								<option value="2"> Karyawan Magang </option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success" form="form_karyawan" name="button_submit">Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<?php include '../template/footer.php'; ?>