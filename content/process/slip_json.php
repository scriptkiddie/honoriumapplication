<?php
	session_start();
	include '../../connection.php';

	if (isset($_POST)) {
		if ($_POST['type'] == 'getMonth') {
			$data = array();
			$year = $_POST['year'];
			$month = mysql_query("SELECT MONTH(periode) as bulan FROM tb_header_absen WHERE YEAR(periode) = '$year' GROUP BY MONTH(periode)");

			$i = 0;
			while ($bulan = mysql_fetch_array($month)) {
				$data[$i]['bulan'] = $bulan['bulan'];
				$i++;
			}
			echo json_encode($data);
		}
		elseif ($_POST['type'] == 'getSlip') {
			// deklarasi variable
			$year = $_POST['year'];
			$month = $_POST['month'];
			$transportasi = 0;
			$lembur = 0;

			// ambil data header join
			$getJoin = "SELECT tb_karyawan.nip,
						tb_karyawan.gaji_pokok,
						tb_karyawan.nama_karyawan,
						tb_karyawan.status_karyawan,
						tb_jabatan.tunjangan_jabatan,
						tb_header_absen.idabsensi,
						tb_header_absen.tj_bpjs,
						tb_header_absen.tj_bonus,
						tb_header_absen.tj_lainnya,
						tb_header_absen.iu_kesehatan,
						tb_header_absen.iu_tenagakerja,
						tb_header_absen.pajak,
						tb_header_absen.potongan
						FROM
						tb_karyawan
						JOIN tb_header_absen ON tb_header_absen.nip = tb_karyawan.nip
						JOIN tb_jabatan ON tb_jabatan.kode_jabatan = tb_karyawan.kode_jabatan
						WHERE tb_karyawan.nip = '" . $_SESSION['nomor_ip'] . "' AND YEAR(tb_header_absen.periode) = '$year' AND MONTH(tb_header_absen.periode) = '$month'";
			$getJoin = mysql_query($getJoin);
			$result = mysql_fetch_array($getJoin);

			// ambil data detail absen
			$getDetail = mysql_query("SELECT * FROM tb_detail_absen WHERE idabsensi = '" . $result['idabsensi'] . "' AND YEAR(tanggal) = '" . $year . "' AND MONTH(tanggal) = '" . $month . "'");
			// echo mysql_error();die();
			while ($detail = mysql_fetch_array($getDetail)) {
				$transportasi += $detail['transport'];
				$lembur += $detail['lembur'];
			}
			$result = array_merge($result, array('transport'=>$transportasi, 'lembur'=>$lembur));
			echo json_encode($result);
		}
		else {
			die();
		}
	}