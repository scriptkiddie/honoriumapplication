<?php
session_start();
include '../connection.php';
require_once '../template/dompdf/autoload.inc.php';

function toRupiah($num){
	return "Rp. " . number_format ($num, 0, ",", ".");
}

// inisialisasi library dompdf
use Dompdf\Dompdf;
$dompdf = new Dompdf();

// deklarasi variable
$year = $_GET['year'];
$month = $_GET['month'];
$transportasi = 0;
$lembur = 0;

// ambil data header join
$getJoin = "SELECT tb_karyawan.nip,
			tb_karyawan.gaji_pokok,
			tb_karyawan.nama_karyawan,
			tb_karyawan.status_karyawan,
			tb_jabatan.tunjangan_jabatan,
			tb_jabatan.nama_jabatan,
			tb_header_absen.idabsensi,
			tb_header_absen.tj_bpjs,
			tb_header_absen.tj_bonus,
			tb_header_absen.tj_lainnya,
			tb_header_absen.iu_kesehatan,
			tb_header_absen.iu_tenagakerja,
			tb_header_absen.pajak,
			tb_header_absen.potongan
			FROM
			tb_karyawan
			JOIN tb_header_absen ON tb_header_absen.nip = tb_karyawan.nip
			JOIN tb_jabatan ON tb_jabatan.kode_jabatan = tb_karyawan.kode_jabatan
			WHERE tb_karyawan.nip = '" . $_SESSION['nomor_ip'] . "' AND YEAR(tb_header_absen.periode) = '$year' AND MONTH(tb_header_absen.periode) = '$month'";
$getJoin = mysql_query($getJoin);
$result = mysql_fetch_array($getJoin);

// ambil data detail absen
$getDetail = mysql_query("SELECT * FROM tb_detail_absen WHERE idabsensi = '" . $result['idabsensi'] . "' AND YEAR(tanggal) = '" . $year . "' AND MONTH(tanggal) = '" . $month . "'");
// echo mysql_error();die();
while ($detail = mysql_fetch_array($getDetail)) {
	$transportasi += $detail['transport'];
	$lembur += $detail['lembur'];
}
$result = array_merge($result, array('transport'=>$transportasi, 'lembur'=>$lembur));

switch ($result['status_karyawan']) {
	case '0':
		$sk = "Karyawan Tetap";
		break;
	
	case "1":
		$sk = "Karyawan Kontrak";
		break;

	default:
		$sk = "Karyawan Magang";
		break;
}

$terima = $result['gaji_pokok'] + $result['tunjangan_jabatan'] + $result['transport'] + $result['tj_bpjs'] + $result['lembur'] + $result['tj_bonus'] + $result['tj_lainnya'];
$pengurangan = $result['iu_kesehatan'] + $result['iu_tenagakerja'] + $result['pajak'] + $result['potongan'];

$html = '<style>td,th{padding:5px}</style><table cellspacing=0 cellpadding="0" width="100%" class="table">
			<tr style="text-align:center;">
				<th colspan="4"><h2>SLIP GAJI</h2></th>
			</tr>
			<tr>
				<td colspan="2"><b>CV SKS DAIICHI REALTY</b></td>
				<td colspan="1">Periode</td>
				<td colspan="1">: ' . date('F', mktime(0, 0, 0, $_GET['month'], 10)) . ' ' . $_GET['year'] . '</td>
			</tr>
			<tr>
				<td colspan="2"</td>
				<td colspan="1">Karyawan</td>
				<td colspan="1">: ' . $result['nama_karyawan'] . '</td>
			</tr>
			<tr>
				<td colspan="2"</td>
				<td colspan="1">Jabatan</td>
				<td colspan="1">: ' . $result['nama_jabatan'] . '</td>
			</tr>
			<tr>
				<td colspan="2"</td>
				<td colspan="1">Status</td>
				<td colspan="1">: ' . $sk . '</td>
			</tr>
			<tr>
				<td colspan="2"</td>
				<td colspan="1">PTKP</td>
				<td colspan="1">: ' . $_SESSION['nomor_ip'] . '</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr style="background-color:#ECF0F1;">
				<th colspan="4">Penerimaan</th>
			</tr>
			<tr>
				<td style="text-align:center">1</td>
				<td>Gaji Pokok</td>
				<td colspan="2" style="text-align:right" id="gapok">' . toRupiah($result['gaji_pokok']) . '</td>
			</tr>
			<tr>
				<td style="text-align:center">2</td>
				<td>Tunjangan Jabatan</td>
				<td colspan="2" style="text-align:right" id="tj_jabatan">' . toRupiah($result['tunjangan_jabatan']) . '</td>
			</tr>
			<tr>
				<td style="text-align:center">3</td>
				<td>Tunjangan Transportasi</td>
				<td colspan="2" style="text-align:right" id="tj_transport">' . toRupiah($result['transport']) . '</td>
			</tr>
			<tr>
				<td style="text-align:center">4</td>
				<td>Tunjangan BPJS</td>
				<td colspan="2" style="text-align:right" id="tj_bpjs">' . toRupiah($result['tj_bpjs']) . '</td>
			</tr>
			<tr>
				<td style="text-align:center">5</td>
				<td>Lembur</td>
				<td colspan="2" style="text-align:right" id="tj_lembur">' . toRupiah($result['lembur']) . '</td>
			</tr>
			<tr>
				<td style="text-align:center">6</td>
				<td>Bonus / THR</td>
				<td colspan="2" style="text-align:right" id="tj_bonus">' . toRupiah($result['tj_bonus']) . '</td>
			</tr>
			<tr>
				<td style="text-align:center">7</td>
				<td>Penerimaan Lainnya</td>
				<td colspan="2" style="text-align:right" id="tj_lainnya">' . toRupiah($result['tj_lainnya']) . '</td>
			</tr>
			<tr style="background-color:grey;color:white;">
				<th colspan="2">Total Penerimaan</th>
				<th colspan="2" style="text-align:right" id="total_penerimaan">' . toRupiah($terima) . '</th>
			</tr>
			<tr style="background-color:#fff;">
				<th colspan="4"></th>
			</tr>
			<tr style="background-color:#ECF0F1;">
				<th colspan="4">Pengurangan</th>
			</tr>
			<tr>
				<td style="text-align:center">1</td>
				<td>Iuran BPJS Kesehatan</td>
				<td colspan="2" style="text-align:right" id="iu_kesehatan">(' . toRupiah($result['iu_kesehatan']) . ')</td>
			</tr>
			<tr>
				<td style="text-align:center">2</td>
				<td>Iuran BPJS Ketenagakerjaan</td>
				<td colspan="2" style="text-align:right" id="iu_tenagakerja">(' . toRupiah($result['iu_tenagakerja']) . ')</td>
			</tr>
			<tr>
				<td style="text-align:center">3</td>
				<td>Pajak</td>
				<td colspan="2" style="text-align:right" id="pajak">(' . toRupiah($result['pajak']) . ')</td>
			</tr>
			<tr>
				<td style="text-align:center">4</td>
				<td>Potongan Telat</td>
				<td colspan="2" style="text-align:right" id="telat">(' . toRupiah($result['potongan']) . ')</td>
			</tr>
			<tr style="background-color:grey;color:white">
				<th colspan="2">Total Pengurangan</th>
				<th colspan="2" style="text-align:right" id="total_pengurangan">(' . toRupiah($pengurangan) . ')</th>
			</tr>
			<tr style="background-color:black;color:white">
				<th colspan="2">Total Diterima Karyawan</th>
				<th colspan="2" style="text-align:right" id="diterima">' . toRupiah($terima - $pengurangan) . '</th>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td align=center colspan="4">Tangerang, 30 ' . date('F', mktime(0, 0, 0, $_GET['month'], 10)) . ' ' . $_GET['year'] .'</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="2">Penerima</td>
				<td align="center" colspan="2">CV SKS Daiichi Realty</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="2">' . $result['nama_karyawan'] . '</td>
				<td align="center" colspan="2">Keuangan</td>
			</tr>
		</table>';

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

?>