<?php 
	session_start();

	include 'connection.php';

	if (isset($_POST['login_button'])) {
		// collecting data into variable
		$username = $_POST['username'];
		$password = $_POST['password'];
		// query cek login
		$getUser = mysql_query("SELECT tb_user.username, tb_user.status, tb_user.password, tb_karyawan.nama_karyawan, tb_karyawan.nip FROM tb_user left join tb_karyawan on tb_user.nip = tb_karyawan.nip WHERE username = '$username' AND password = '$password'");
		if(is_resource($getUser)){
			$data = mysql_fetch_array($getUser);
			if (mysql_num_rows($getUser) > 0) {
				$_SESSION['username'] = $data['username'];
				$_SESSION['nomor_ip'] = $data['nip'];
				$_SESSION['nama_karyawan'] = $data['nama_karyawan'];
				$_SESSION['status'] = $data['status'];
				$_SESSION['password'] = $data['password'];
				header('Location: content/beranda.php');
			}
			else {
				echo '<script type="text/javascript">alert("Login salah!");</script>';
			}
		}else{
			echo '<script type="text/javascript">alert("Login salah!");</script>';
		}
	}else if(isset($_SESSION['username']) && isset($_SESSION['password'])){
		header('Location: content/beranda.php');
	}
 ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login | <?php echo APP_NAME ?></title>

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form" method="post" action="">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus="" required>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="" required>
							</div>
							<input type="submit" name="login_button" class="btn btn-primary" value="Login">
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
</body>

</html>
