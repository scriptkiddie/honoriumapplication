# Aplikasi Penggajian Karyawan #

Aplikasi ini dibuat untuk memudahkan sebuah perusahaan dalam menentukan gaji/upah para karyawannya. Aplikasi ini masih bersifat dasar, jadi tidak terhubung ke mesin absen apapun dan masih menggunakan metode input manual.

### Apa yang ada di dalam aplikasi ini? ###

* Data master karyawan
* Data master jabatan
* Transaksi penginputan honor atau gaji
* Laporan - laporan.