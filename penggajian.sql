-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Jun 2017 pada 01.58
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `penggajian`
--
CREATE DATABASE IF NOT EXISTS `penggajian` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `penggajian`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_absen`
--

DROP TABLE IF EXISTS `tb_detail_absen`;
CREATE TABLE IF NOT EXISTS `tb_detail_absen` (
  `idabsensi` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `masuk` time DEFAULT NULL,
  `keluar` time DEFAULT NULL,
  `transport` int(11) DEFAULT NULL,
  `lembur` int(11) DEFAULT NULL,
  `ket_telat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_detail_absen`
--

INSERT INTO `tb_detail_absen` (`idabsensi`, `tanggal`, `masuk`, `keluar`, `transport`, `lembur`, `ket_telat`) VALUES
(1, '2017-06-01', '08:00:00', '17:00:00', 15000, 0, ''),
(1, '2017-06-02', '08:00:00', '17:00:00', 15000, 0, ''),
(1, '2017-06-03', '08:00:00', '17:00:00', 15000, 0, ''),
(1, '2017-06-04', '08:00:00', '17:00:00', 15000, 0, ''),
(1, '2017-06-05', '08:00:00', '17:00:00', 15000, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_header_absen`
--

DROP TABLE IF EXISTS `tb_header_absen`;
CREATE TABLE IF NOT EXISTS `tb_header_absen` (
`idabsensi` int(11) NOT NULL,
  `nip` varchar(10) DEFAULT NULL,
  `periode` date DEFAULT NULL,
  `tj_bpjs` int(11) DEFAULT NULL,
  `tj_bonus` int(11) DEFAULT NULL,
  `tj_lainnya` int(11) DEFAULT NULL,
  `iu_kesehatan` int(11) DEFAULT NULL,
  `iu_tenagakerja` int(11) DEFAULT NULL,
  `pajak` int(11) DEFAULT NULL,
  `potongan` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `tb_header_absen`
--

INSERT INTO `tb_header_absen` (`idabsensi`, `nip`, `periode`, `tj_bpjs`, `tj_bonus`, `tj_lainnya`, `iu_kesehatan`, `iu_tenagakerja`, `pajak`, `potongan`) VALUES
(1, '112', '2017-06-01', 220000, 0, 0, 90000, 145000, 0, 100000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jabatan`
--

DROP TABLE IF EXISTS `tb_jabatan`;
CREATE TABLE IF NOT EXISTS `tb_jabatan` (
  `kode_jabatan` varchar(10) NOT NULL,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  `tunjangan_jabatan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`kode_jabatan`, `nama_jabatan`, `tunjangan_jabatan`) VALUES
('000', 'HRD', 300000),
('001', 'Manager', 500000),
('002', 'Staff Keuangan', 200000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_karyawan`
--

DROP TABLE IF EXISTS `tb_karyawan`;
CREATE TABLE IF NOT EXISTS `tb_karyawan` (
  `nip` varchar(10) NOT NULL,
  `nama_karyawan` varchar(50) DEFAULT NULL,
  `alamat` text,
  `email` varchar(20) DEFAULT NULL,
  `telepon` varchar(13) DEFAULT NULL,
  `jenis_kelamin` int(11) DEFAULT NULL,
  `kode_jabatan` varchar(10) DEFAULT NULL,
  `gaji_pokok` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `tgl_sp` date DEFAULT NULL,
  `status_karyawan` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`nip`, `nama_karyawan`, `alamat`, `email`, `telepon`, `jenis_kelamin`, `kode_jabatan`, `gaji_pokok`, `status`, `tgl_sp`, `status_karyawan`) VALUES
('0', 'HRD', 'Jl. Rawa rawa', 'm@mm.mm', '0123', 2, '000', 3400000, 0, '0000-00-00', 0),
('111', 'Nita', 'Kampung Pulo', 'nita@nhn.co', '121112223', 2, '001', 3400000, 0, '0000-00-00', 0),
('112', 'Agus', 'Karawang', 'agus@pt.com', '012345678901', 1, '002', 3400000, 1, '2017-05-01', 0),
('113', 'Ridwan Nugraha', 'Poso', '123@mm.mm', '098123', 1, '001', 3400000, 0, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE IF NOT EXISTS `tb_user` (
`id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `nip` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `status`, `nip`) VALUES
(2, 'hrd', '123', 1, '0'),
(3, 'nita', '123', 2, '111'),
(4, 'agus', '123', 2, '112');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_detail_absen`
--
ALTER TABLE `tb_detail_absen`
 ADD KEY `fk_absensi` (`idabsensi`);

--
-- Indexes for table `tb_header_absen`
--
ALTER TABLE `tb_header_absen`
 ADD PRIMARY KEY (`idabsensi`), ADD KEY `fk_nip` (`nip`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
 ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
 ADD PRIMARY KEY (`nip`), ADD KEY `fk_jabatan` (`kode_jabatan`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
 ADD PRIMARY KEY (`id`), ADD KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_header_absen`
--
ALTER TABLE `tb_header_absen`
MODIFY `idabsensi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_detail_absen`
--
ALTER TABLE `tb_detail_absen`
ADD CONSTRAINT `fk_absensi` FOREIGN KEY (`idabsensi`) REFERENCES `tb_header_absen` (`idabsensi`);

--
-- Ketidakleluasaan untuk tabel `tb_header_absen`
--
ALTER TABLE `tb_header_absen`
ADD CONSTRAINT `fk_nip` FOREIGN KEY (`nip`) REFERENCES `tb_karyawan` (`nip`);

--
-- Ketidakleluasaan untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
ADD CONSTRAINT `fk_jabatan` FOREIGN KEY (`kode_jabatan`) REFERENCES `tb_jabatan` (`kode_jabatan`);

--
-- Ketidakleluasaan untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `tb_karyawan` (`nip`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
