<?php
session_start();
if(!isset($_SESSION['username']) && !isset($_SESSION['password'])){
	header('Location: ..');
}
if(isset($_GET['logout'])){
	session_destroy();
	header('Location: ..');
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo APP_NAME ?></title>

	<?php include 'css_mandatory.php'; ?>

	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>