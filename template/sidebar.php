<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
	<hr>
	<ul class="nav menu">
		<?php
			if($_SESSION['status'] == 1){
				$menus = array(
						'dashboard' => array(
									'link' => 'beranda.php',
									'title' => 'Beranda',
									'icon' => 'glyphicon glyphicon-home'
									),
						'transaction' => array(
									'link' => 'transaksi.php',
									'title' => 'Transaksi',
									'icon' => 'glyphicon glyphicon-usd'
									),
						);
			}else{
				$menus = array(
						'dashboard' => array(
									'link' => 'beranda.php',
									'title' => 'Beranda',
									'icon' => 'glyphicon glyphicon-home'
									),
						'transaction' => array(
									'link' => 'slip.php',
									'title' => 'Lihat Slip Gaji',
									'icon' => 'glyphicon glyphicon-usd'
									),
						);
			}
			$subMaster = array(
						'employee' => array(
									'link' => 'karyawan.php',
									'title' => 'Karyawan',
									'icon' => 'glyphicon glyphicon-bookmark'
									),
						'grade' => array(
									'link' => 'jabatan.php',
									'title' => 'Jabatan',
									'icon' => 'glyphicon glyphicon-certificate'
									),
						'user' => array(
									'link' => 'pengguna.php',
									'title' => 'Pengguna',
									'icon' => 'glyphicon glyphicon-user'
									),

						);
			foreach ($menus as $key => $value) {
		?>
			<li>
				<a href="<?php echo $value['link'] ?>">
					<i class="<?php echo $value['icon'] ?>"></i>&emsp;<?php echo $value['title'] ?>
				</a>
			</li>
		<?php
			}

			if($_SESSION['status'] == 1){
		?>
		<li class="parent ">
			<a href="javascript:;">
				<span data-toggle="collapse" href="#sub-item-1">
					<i class="glyphicon glyphicon-chevron-down"></i>&emsp;Data Master
				</span>
			</a>
			<ul class="children collapse" id="sub-item-1">
				<?php foreach ($subMaster as $key => $value): ?>
					<li>
						<a class="" href="<?php echo $value['link'] ?>">
							<i class="<?php echo $value['icon'] ?>"></i>&emsp;<?php echo $value['title'] ?>
						</a>
					</li>
				<?php endforeach ?>
			</ul>
		</li>
		<?php } ?>
		<li role="presentation" class="divider"></li>
	</ul>

</div><!--/.sidebar-->