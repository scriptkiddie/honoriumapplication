<!--Icons-->
<script src="../assets/js/lumino.glyphs.js"></script>
<script src="../assets/js/jquery-1.11.1.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
	// NUMBER FORMATTING
	Number.prototype.format = function(n, x) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
	};
	String.prototype.unformat = function() {
	    return this.replace("Rp. ", "").replace(/\./g, "");
	};

	// Array Remover By Value
	Array.prototype.removeByVal = function() {
	    var what, a = arguments, L = a.length, ax;
	    while (L && this.length) {
	        what = a[--L];
	        while ((ax = this.indexOf(what)) !== -1) {
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	};

	// Date Formatter as YYYY/mm/dd
	Date.prototype.shortDate = function() {
		var mm = this.getMonth() + 1; // getMonth() is zero-based
		var dd = this.getDate();

		return [this.getFullYear(),
		      (mm>9 ? '' : '0') + mm,
		      (dd>9 ? '' : '0') + dd
		     ].join('/');
	};
</script>
<?php if(isset($js_external)) include "$js_external"; ?>
<script type="text/javascript">
	!function ($) {
	    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
	        $(this).find('em:first').toggleClass("glyphicon-minus");      
	    }); 
	    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
	}(window.jQuery);

	$(window).on('resize', function () {
	  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
	})
	$(window).on('resize', function () {
	  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
	})
</script>