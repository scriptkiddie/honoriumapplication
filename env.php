<?php
	/**
	 * -------------------------------------------------------------------
	 * SETTING UP VARIABLE CONSTANT FOR APPLICATION
	 * -------------------------------------------------------------------
	 */

	date_default_timezone_set("Asia/Jakarta");

	/**
	 * -------------------------------------------------------------
	 * PATH AND DIRECTORIES CONFIG
	 * -------------------------------------------------------------
	 */
		define('SERVER_NAME'      , $_SERVER['SERVER_NAME']);

	/**
	 * ---------------------------------------------------------------
	 * APPLICATION CONFIG
	 * ---------------------------------------------------------------
	 */
		// setup App name
		define('APP_NAME'    , 'Aplikasi Penggajian Karyawan');
		// setuo App version
		define('APP_VERSION' , '1.0');
		// setup App owner
		define('APP_OWNER'   , 'Arif Samsudi');
		// setup App logo
		define('APP_LOGO'    , ROOT_URL . '/assets/images/logo-small.png');
		// setup App fav icon
		define('APP_FAVICON' , ROOT_URL . '/assets/images/favicon.png');

	/**
	 * --------------------------------------------------------------
	 * DATABASE CONFIG
	 * --------------------------------------------------------------
	 */

		// setup database host
		define('DB_HOST'    , SERVER_NAME);
		// setup database name
		define('DB_DATABASE', 'penggajian');
		// setup username
		define('DB_USERNAME', 'root');
		// setup password
		define('DB_PASSWORD', '');